package it.uniroma2.pjdm.esempiogridview;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.ImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by clauz on 5/21/18.
 */

public class Comic {
    private String title = "lorem ipsum";
    private String img = "";
    private Bitmap bmp = null;
    private int num = 0;
    private ComicAdapter adapter = null;
    private AsyncTask task = null;
    private boolean loaded = false;
    private String json = "";
    private Context ctx = null;

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public void setAdapter(ComicAdapter adapter) {
        this.adapter = adapter;

        if(!this.loaded && getNum() != 0) {
            load();
        }

        if(!this.loaded && this.task == null) {
            this.task = new RetrieveComic().execute(this);
        }
    }

    public ComicAdapter getAdapter() {
        return this.adapter;
    }

    public Comic(Context ctx) {
        this.bmp = BitmapFactory.decodeResource(ctx.getResources(), R.raw.xkcd);
        this.ctx = ctx;
    }

    @Override
    public String toString() {
        return getTitle();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public Bitmap getBmp() {
        return bmp;
    }

    public void setBmp(Bitmap bmp) {
        this.bmp = bmp;
    }

    public void parseJSON(String json) throws JSONException {
        JSONObject jo = new JSONObject(json);
        this.setTitle(jo.getString("title"));
        this.setImg(jo.getString("img"));
        this.json = json;
    }

    public String toJSON() {
        return this.json;
    }

    private String getFilename(String suffix) {
        return "xkcd_" + this.getNum() + suffix;
    }

    public void save() {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this.ctx);
        boolean use_cache = sharedPref.getBoolean("use_cache", true);
        if(!use_cache) {
            return;
        }

        try {
            File jsonFile =
                    new File(this.ctx.getCacheDir(), getFilename(".json"));
            BufferedWriter writer = new BufferedWriter(new FileWriter(jsonFile));
            writer.write(this.toJSON());
            writer.close();

            File imgFile =
                    new File(this.ctx.getCacheDir(), getFilename(".png"));
            FileOutputStream fos = new FileOutputStream(imgFile);
            this.getBmp().compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.close();
        } catch (IOException e) {
            Log.w("CLA", "Something is wrong with the cache file writing");
            e.printStackTrace();
        }
    }

    public void load() {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this.ctx);
        boolean use_cache = sharedPref.getBoolean("use_cache", true);
        if(!use_cache) {
            return;
        }

        try {
            /* retrieve the JSON */
            Log.d("CLA", "cache directory " + ctx.getCacheDir());
            File jsonFile = new File(ctx.getCacheDir(), getFilename(".json"));
            FileInputStream fis = new FileInputStream(jsonFile);
            BufferedReader br = new BufferedReader(new InputStreamReader(fis));
            String json = "";
            String line = br.readLine();
            while (line != null) {
                json += line;
                line = br.readLine();
            }
            br.close();
            fis.close();
            this.parseJSON(json);

            /* retrieve the image */
            File imgFile = new File(ctx.getCacheDir(), getFilename(".png"));
            fis = new FileInputStream(imgFile);
            this.setBmp(BitmapFactory.decodeStream(fis));
            fis.close();

            this.loaded = true;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public boolean saveBmp() {
        boolean result = false;
        String state = Environment.getExternalStorageState();
        if(Environment.MEDIA_MOUNTED.equals(state)) {
            ActivityCompat.requestPermissions(
                    (Activity) this.ctx,
                    new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    1
            );
            File bmpFile = new File(
                    Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                    this.getFilename(".png")
            );
            try {
                FileOutputStream fos = new FileOutputStream(bmpFile);
                this.getBmp().compress(Bitmap.CompressFormat.PNG, 100, fos);
                fos.close();
                result = true;
            } catch (IOException e) {
                e.printStackTrace();
                result = false;
            }

        }
        return result;
    }

    private class RetrieveComic extends AsyncTask<Comic, Void, Comic> {

        @Override
        protected Comic doInBackground(Comic... comics) {
            Comic comic = comics[0];
            int num = comic.getNum();

            try {
                Log.d("CLA", "retrieve " + num);
                URL url = new URL("https://xkcd.com/" + num  + "/info.0.json");
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                InputStream is = urlConnection.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is));

                /* read the JSON*/
                String json = "";
                String line = reader.readLine();
                while(line != null) {
                    json += line;
                    line = reader.readLine();
                }

                comic.parseJSON(json);

                url = new URL(comic.getImg());
                urlConnection = (HttpURLConnection) url.openConnection();
                is = urlConnection.getInputStream();
                Bitmap bitmap = BitmapFactory.decodeStream(is);
                comic.setBmp(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return comic;
        }

        @Override
        protected void onPostExecute(Comic comic) {
            Log.d("CLA", "onpostexecute");
            loaded = true;
            save();
            adapter.notifyDataSetChanged();
        }
    }
















}
