package it.uniroma2.pjdm.esempiogridview;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by clauz on 5/21/18.
 */

public class ComicAdapter extends ArrayAdapter {
    private LayoutInflater inflater;
    private Comic[] comics;

    public ComicAdapter(@NonNull Context context, int resource, @NonNull Comic[] comics) {
        super(context, resource);
        this.comics = comics;
        this.inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        comics[position].setAdapter(this);
        View v = convertView;
        if(convertView == null) {
            v = inflater.inflate(R.layout.gridlayout, parent, false);
        }
        TextView textView = v.findViewById(R.id.comictextview);
        textView.setText(comics[position].getTitle());
        ImageView imageView = v.findViewById(R.id.comicimageview);
        imageView.setImageBitmap(comics[position].getBmp());
        Log.d("CLA", "getview");
        return v;
    }

    @Override
    public int getCount() {
        return comics.length;
    }
}










