package it.uniroma2.pjdm.esempiogridview;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private int nelements = 250;
    private Comic[] comics = new Comic[nelements];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        for(int i = 0; i < nelements; i++) {
            comics[i] = new Comic(this);
            comics[i].setNum(i + 1);
        }

        ComicAdapter comicAdapter = new ComicAdapter(this,
                android.R.layout.simple_list_item_1,
                comics);

        GridView gridview1 = findViewById(R.id.grid1);
        gridview1.setAdapter(comicAdapter);

        gridview1.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                if(comics[i].saveBmp()) {
                    Toast.makeText(MainActivity.this, "image saved", Toast.LENGTH_LONG).show();
                }
                return true;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    public void launchSettings(MenuItem item) {
        Intent settingsIntent = new Intent();
        settingsIntent.setClass(this, SettingsActivity.class);
        startActivity(settingsIntent);
    }
}
